from django.contrib import messages
from django.urls import reverse_lazy
import requests
from django.shortcuts import redirect, render

from userapp.forms import UserForm
from userapp.task import email_confirm
from userapp.web_scraping import web_request_scraping


# Create your views here.

def welcome(request):
    return render(request, 'index.html')

def users_list(request):
    resp = requests.get("http://localhost:8000/list_user/list_view")
    data = resp.json()

    return render(request, 'users_list.html', {'data': data})

def create_user(request):
    if request.method == 'POST':
        try:
            data = request.POST.dict()
            requests.post("http://localhost:8000/list_user/list_view", data=data)
            messages.success(request, 'User Created Successfully')
            email_confirm.delay(data)
            return redirect(reverse_lazy('users_list'))
        except:
            messages.error(request, 'Error al crear el usuario')

    else:
        form = UserForm()
    return render(request, 'create_user.html', {'form': form})

def update_user(request, id):
    if request.method == 'POST':
        data = request.POST.dict()
        requests.put(f"http://localhost:8000/list_user/detail_view/{id}", data=data)
        return redirect(reverse_lazy('users_list'))
    elif request.method == 'GET':
        resp = requests.get(f"http://localhost:8000/list_user/detail_view/{id}")
        data = resp.json()
        form = UserForm(data)
    return render(request, 'update_user.html', {'form': form, 'data': data})

def delete_user(request, id):
    requests.delete(f"http://localhost:8000/list_user/detail_view/{id}")
    return redirect(reverse_lazy('users_list'))

def colombian_soccer_statistics(request):
    resp = web_request_scraping()
    return render(request, 'colombian_soccer_statistics.html', {'data': resp})