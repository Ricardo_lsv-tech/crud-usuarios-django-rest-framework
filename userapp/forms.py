from django import forms
from usuarios.models import ModelUser

class UserForm(forms.ModelForm):
    class Meta:
        model = ModelUser
        fields = ['username', 'name', 'lastname', 'cellphone', 'email']
    
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}), label='Nombre de usuario')
    name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}), label='Nombre')
    lastname = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}), label='Apellido')
    cellphone = forms.ImageField(widget=forms.TextInput(attrs={'class':'form-control'}), label='Celular')
    email = forms.EmailField(widget=forms.TextInput(attrs={'class':'form-control'}), label='Correo electrónico')
    