# Create your tasks here
from datetime import datetime
import time
from django.conf import settings
from celery import shared_task
from django.core.mail import send_mail
from django.template.loader import render_to_string


@shared_task
def email_confirm(data):
    time.sleep(5)
    send_mail(
        'Confirm Registration',
        'User registered Successful',
        settings.EMAIL_HOST_USER,
        [data['email']],
        fail_silently=False,
        html_message=render_to_string('email.html', {'username': data['username'], 'name': data['name'],
                                                     'lastname': data['lastname'], 'email': data['email']})
    )
