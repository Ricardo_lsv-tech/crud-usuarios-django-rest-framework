import requests
import pandas as pd
from bs4 import BeautifulSoup

def web_request_scraping():
    url = 'https://www.eltiempo.com/deportes/futbol-colombiano/tabla-posiciones'
    response = requests.get(url)
    html = response.text
    soup = BeautifulSoup(html, 'html.parser')

    # Equipos
    tb_eq = soup.find('table').find('tbody').find_all('tr')

    posiciones = []
    equipos = []
    pj = []
    pg = []
    pp = []
    pe = []
    gf = []
    gc = []
    fv = []
    cv = []
    gd = []
    pts = []
    for equipo in tb_eq:
        posiciones.append(equipo.find_all('td')[0].text)
        equipos.append(equipo.find_all('td')[2].text)
        pj.append(equipo.find_all('td')[3].text)
        pg.append(equipo.find_all('td')[4].text)
        pp.append(equipo.find_all('td')[5].text)
        pe.append(equipo.find_all('td')[6].text)
        gf.append(equipo.find_all('td')[7].text)
        gc.append(equipo.find_all('td')[8].text)
        fv.append(equipo.find_all('td')[9].text)
        cv.append(equipo.find_all('td')[10].text)
        gd.append(equipo.find_all('td')[11].text)
        pts.append(equipo.find_all('td')[12].text)

    df = pd.DataFrame({'Posicion': posiciones, 'Equipo': equipos, 'PJ': pj, 'PG': pg, 'PP': pp, 'PE': pe, 'GF': gf, 'GC': gc, 'FV': fv, 'CV': cv, 'GD': gd, 'PTS': pts})
    data = df.to_dict(orient='records')
    
    return data