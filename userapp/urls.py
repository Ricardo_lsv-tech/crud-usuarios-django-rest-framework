from userapp import views
from django.urls import path

urlpatterns = [
    path('', views.welcome, name='index'),
    path('list_user', views.users_list, name='users_list'),
    path('create_user', views.create_user, name='create_user'),
    path('update_user/<int:id>', views.update_user, name='update_user'),
    path('delete_user/<int:id>', views.delete_user, name='delete_user'),
    path('statistics', views.colombian_soccer_statistics, name='statistics'),

]