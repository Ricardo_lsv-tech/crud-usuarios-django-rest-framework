import datetime
import json
from unittest import mock
import django
django.setup()

from django.core import mail
from django.test import TestCase

from django.urls import reverse_lazy, reverse

from usuarios.models import ModelUser
from userapp.forms import UserForm



class UserAppViewTest(TestCase):
    def setUp(self):
        """
        Setup for test
        """
        mocked_dt = datetime.datetime(2022, 9, 9).strftime("%Y-%m-%dT%H:%M:%SZ")
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_dt)):
            user = ModelUser(
                user_id=1,
                username='andresmolinares',
                name='Andres',
                lastname='Molinares',
                cellphone='619191',
                email='andresm@mail.com',
            )
            user.save()

    def test_user_list_empty(self):
        url = reverse_lazy('user_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)

    def test_create_user(self):
        url = reverse_lazy('user_list')
        data = {
            'username': 'andresmolinares2',
            'name': 'Andres',
            'lastname': 'Molinares',
            'cellphone': '+573015109237',
            'email': 'andresmolinares@gmail.com',
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(ModelUser.objects.count(), 2)
        self.assertEqual(ModelUser.objects.get(username='andresmolinares2').username, 'andresmolinares2')


    def test_update_user(self):
        """
        Test PUT update user 200
        """
        
        pk=1
        url = reverse_lazy('user_detail', kwargs={'pk': pk})
        data = {
            'username': 'andresmolinares2',
            'name': 'Andres Felipe',
            'lastname': 'Molinares',
            'cellphone': '+573015109237',
            'email': 'a@a.com',
        }
        response = self.client.put(url, data, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(ModelUser.objects.get(username='andresmolinares2').email, 'a@a.com')

class ScrapperTest(TestCase):
    def test_scrapper_list(self):
        url = reverse_lazy('statistics')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

