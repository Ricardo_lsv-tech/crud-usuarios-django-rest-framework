from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from usuarios.models import ModelUser
from usuarios.user_serializer import UserSerializer

# Create your views here.

@api_view(['GET', 'POST'])
def user_api_view(request):
    if request.method == 'GET':
        user_list = ModelUser.objects.all()
        serializer = UserSerializer(user_list, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

@api_view(['GET', 'PUT', 'DELETE'])
def user_detail_api_view(request, pk):
    try:
        user = ModelUser.objects.get(user_id=pk)
    except ModelUser.DoesNotExist:
        return Response(status=404)

    if request.method == 'GET':
        serializer = UserSerializer(user)
        return Response(serializer.data)
    elif request.method == 'PUT':
        serializer = UserSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)
    elif request.method == 'DELETE':
        user.delete()
        return Response(status=204)
