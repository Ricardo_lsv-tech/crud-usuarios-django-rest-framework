import django
django.setup()

from django.db import IntegrityError
from django.test import TransactionTestCase

from usuarios.models import ModelUser

import os,django
os.environ.setdefault ("DJANGO_SETTINGS_MODULE", "user_crud.settings") # project_name nombre del proyecto
django.setup()



class UserModelTest(TransactionTestCase):
    def test_email_mandatory(self):
        """
        Test email mandatory
        """
        user = ModelUser(
            username='test',
            name='test',
            lastname='test',
            cellphone='test',
        )
        with self.assertRaises(IntegrityError):
            user.save()
        values_error = [
            '',
            'test@',
            'test@test',
            'test@test.',
            'test'
            '   ',
        ]
        for value in values_error:
            user.email = value
            with self.assertRaises(IntegrityError, msg=f"{value} is not a valid email"):
                user.save()
        user.email = 'test@email.com'
        user.save()
        db_user = ModelUser.objects.get(email=user.email)
        self.assertEqual(user.email, db_user.email)

    def test_username_unique(self):
        """
        Test username unique
        """
        user_test1 = ModelUser(
            username='usuarioTester',
            name='Andres',
            lastname='Molinares',
            cellphone='619191',
            email='fmolinares@lsv-tech.com',

        )
        user_test1.save()
        user_test2 = ModelUser(
            username='usuarioTester',
            name='Ricardo',
            lastname='Jaramillo',
            cellphone='13213574',
            email='rjaramillo@lsv-tech.com',

        )

        with self.assertRaisesMessage(IntegrityError, expected_message="UNIQUE constraint failed: usuarios_modeluser.username"):
            user_test2.save()

    def test_email_unique(self):
        """
        Test email unique
        """
        user_test1 = ModelUser(
            username='andresmolinares',
            name='Andres',
            lastname='Molinares',
            cellphone='619191',
            email='administrador@lsv-tech.com',

        )
        user_test1.save()
        user_test2 = ModelUser(
            username='ricardojaramillo',
            name='Ricardo',
            lastname='Jaramillo',
            cellphone='13213574',
            email='administrador@lsv-tech.com',

        )

        with self.assertRaisesMessage(IntegrityError, expected_message="UNIQUE constraint failed: usuarios_modeluser.email"):
            user_test2.save()


