import django
django.setup()

from django.urls import reverse_lazy
from unittest import mock
import datetime

import os,django
os.environ.setdefault ("DJANGO_SETTINGS_MODULE", "user_crud.settings") # project_name nombre del proyecto
django.setup()


from rest_framework import status

from usuarios.models import ModelUser
import json
from rest_framework.test import APITestCase, URLPatternsTestCase, APIClient


class UserSerializerTest(APITestCase):
    def setUp(self):
        """
        Setup for test
        """
        mocked_dt = datetime.datetime(2022, 9, 9).strftime("%Y-%m-%dT%H:%M:%SZ")
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_dt)):
            user = ModelUser(
                user_id=1,
                username='andresmolinares',
                name='Andres',
                lastname='Molinares',
                cellphone='619191',
                email='andresm@mail.com',
            )
            user.save()


    def test_user_list(self):
        """
        Test 200  OK - List User found
        """
        url = reverse_lazy('user_list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)

    def test_user_detail_not_found(self):
        """
        Test 404 Not User Found
        """
        url = reverse_lazy('user_detail', kwargs={'pk': 3})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_user_detail_found(self):
        """
        Test 200  OK - User found
        """
        url = reverse_lazy('user_detail', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data,
                         {'user_id': 1,
                          'username': 'andresmolinares',
                          'name': 'Andres',
                          'lastname': 'Molinares',
                          'cellphone': '619191',
                          'email': 'andresm@mail.com',
                          'created_at': datetime.datetime(2022, 9, 9).strftime("%Y-%m-%dT%H:%M:%SZ"),
                          'updated_at': datetime.datetime(2022, 9, 9).strftime("%Y-%m-%dT%H:%M:%SZ")
                          })

    def test_create_user(self):
        """
        Test POST create user 201
        """
        url = reverse_lazy('user_list')
        data = {
            'username': 'andresmolinares2',
            'name': 'Andres',
            'lastname': 'Molinares',
            'cellphone': '+573015109237',
            'email': 'andresmolinares@gmail.com',
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(ModelUser.objects.count(), 2)
        self.assertEqual(ModelUser.objects.get(username='andresmolinares2').username, 'andresmolinares2')

    def test_user_not_created(self):
        """
        Test POST create user 400
        """
        url = reverse_lazy('user_list')
        data = {
            'username': 'andresmolinares2',
            'name': 'Andres',
            'cellphone': '+573015109237',
            'email': 'sample@test.com',
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(ModelUser.objects.count(), 1)

    def test_update_user(self):
        """
        Test PUT update user 200
        """
        url = reverse_lazy('user_detail', kwargs={'pk': 1})
        data = {
            'username': 'andresmolinares2',
            'name': 'Andres',
            'lastname': 'Molinares',
            'cellphone': '+573015109237',
            'email': 'a@a.com',
        }
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(ModelUser.objects.get(username='andresmolinares2').email, 'a@a.com')



    def test_delete_user(self):
        """
        Testing API delete users 204
        """
        url = reverse_lazy('user_detail', kwargs={'pk': 1})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(ModelUser.objects.count(), 0)