from django.urls import path
from usuarios.views import user_api_view, user_detail_api_view


urlpatterns = [
    path('list_view', user_api_view, name='user_list'),
    path('detail_view/<int:pk>', user_detail_api_view, name='user_detail'),

]