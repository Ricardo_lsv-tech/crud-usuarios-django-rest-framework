from django.contrib import admin

from usuarios.models import ModelUser

# Register your models here.
@admin.register(ModelUser)
class ModeluserAdmin(admin.ModelAdmin):
    list_display = ('username', 'name', 'lastname', 'cellphone', 'email', 'created_at', 'updated_at')
    search_fields = ('username', 'name', 'lastname', 'cellphone', 'email')
    list_filter = ('created_at',)
    ordering = ('-created_at',)