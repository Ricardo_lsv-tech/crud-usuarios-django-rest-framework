import datetime
from tabnanny import verbose

from django.core.exceptions import ValidationError
from django.db import models, IntegrityError
from django.core.validators import validate_email
# Create your models here.



class ModelUser(models.Model):
    user_id = models.AutoField(primary_key=True)
    username = models.CharField(
        max_length=50,
        unique=True,
        null=False,
        blank=False,
        default=None,
    )
    name = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    cellphone = models.CharField(max_length=20)
    email = models.EmailField(
        max_length=100,
        null=False,
        blank=False,
        default= None,
        unique=True,
        validators=[validate_email])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        try:
            validate_email(self.email)
        except ValidationError as error:
            raise IntegrityError(error.message)
        super().save(*args, **kwargs)
    class Meta:
        verbose_name = 'Modelo de usuario'
        verbose_name_plural = 'Modelo de usuarios'