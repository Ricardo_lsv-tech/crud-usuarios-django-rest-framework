from rest_framework import serializers
from usuarios.models import ModelUser

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelUser
        fields = ('user_id','username', 'name', 'lastname', 'cellphone', 'email', 'created_at', 'updated_at')