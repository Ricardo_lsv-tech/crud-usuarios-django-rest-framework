axios.defaults.xsrfCookieName = "csrftoken";
axios.defaults.xsrfHeaderName = "X-CSRFToken";

var tbody_list = document.getElementById("tbody_users");
var formUsers = document.getElementById("form_user");
var formUserEdit = document.getElementById("form_user_edit");
var tbody_content = "";
async function get_users() {
  document.getElementById("list_users").classList.remove("d-none");
  tbody_content = "";
  await axios
    .get("http://localhost:8000/list_user/list_view")
    .then(function (resp) {
      resp.data.forEach(function (user) {
        tbody_content += `<tr>
              <td>${user.user_id}</td>
              <td>${user.username}</td>
              <td>${user.name}</td>
              <td>${user.lastname}</td>
              <td>${user.cellphone}</td>
              <td>${user.email}</td>
              <td>${user.created_at}</td>
              <td>${user.updated_at}</td>
            <td>
            <button class="btn btn-success btn-sm" onclick="edit_form(${user.user_id})"><i class="fa-solid fa-pen-to-square"></i></button>
            <button class="btn btn-danger btn-sm" onclick="delete_user(${user.user_id})"><i class="fa-solid fa-trash-can"></i></button></td>
              </tr>`;
      });
      tbody_list.innerHTML = tbody_content;
    })
    .catch(function (err) {
      console.log(err);
    });
}

function form_users() {
  document.getElementById("form_main").classList.remove("d-none");
  document.getElementById("form_main_edit").classList.add("d-none");
  document.getElementById("list_users").classList.add("d-none");
}

function create_user() {
  formUsers.addEventListener("submit", function (e) {
    e.preventDefault();
    var formData = new FormData(formUsers);

    let data = {
      username: formData.get("username"),
      name: formData.get("name"),
      lastname: formData.get("lastname"),
      cellphone: formData.get("cellphone"),
      email: formData.get("email"),
    };
    axios
      .post("http://localhost:8000/list_user/list_view", data)
      .then(function (resp) {
        console.log("enviado correctamente");
        document.getElementById("form_main").classList.add("d-none");
        document.getElementById("form_main_edit").classList.add("d-none");
        document.getElementById("list_users").classList.remove("d-none");

        get_users();
      })
      .catch(function (err) {
        console.log(err);
      });
    e.target.reset();
  });
}

function edit_form(user_id) {
  axios
    .get(`http://localhost:8000/list_user/detail_view/${user_id}`)
    .then(function (resp) {
      document.getElementById("list_users").classList.add("d-none");
      document.getElementById("form_main").classList.add("d-none");
      document.getElementById("form_main_edit").classList.remove("d-none");
      document.querySelector("#username_edit").value = resp.data.username;
      document.querySelector("#name_edit").value = resp.data.name;
      document.querySelector("#lastname_edit").value = resp.data.lastname;
      document.querySelector("#cellphone_edit").value = resp.data.cellphone;
      document.querySelector("#email_edit").value = resp.data.email;
      document.querySelector("#submit").setAttribute("onclick", `edit_user(${user_id})`);
      edit_user(resp.data.user_id);
    })
    .catch(function (err) {
      console.log(err);
    });
}

function edit_user(data_id) {
  console.log(data_id);
  let data_user_id = data_id;
  formUserEdit.addEventListener("submit", function (e) {
    e.preventDefault();
    var formData = new FormData(formUserEdit);
    let data = {
      username: formData.get("username_edit"),
      name: formData.get("name_edit"),
      lastname: formData.get("lastname_edit"),
      cellphone: formData.get("cellphone_edit"),
      email: formData.get("email_edit"),
    };
    console.log(data);
    axios
      .put(`http://localhost:8000/list_user/detail_view/${data_user_id}`, data)
      .then(function (resp) {
        console.log("enviado correctamente");
        document.getElementById("form_main_edit").classList.add("d-none");
        document.getElementById("form_main").classList.add("d-none");
        document.getElementById("list_users").classList.remove("d-none");
        get_users();
      })
      .catch(function (err) {
        console.log(err);
      });
    e.target.reset();
  });
}

function delete_user(id) {
  axios
    .delete(`http://localhost:8000/list_user/detail_view/${id}`)
    .then(function (resp) {
      console.log("eliminado correctamente");
      get_users();
    })
    .catch(function (err) {
      console.log(err);
    });
}

get_users();
