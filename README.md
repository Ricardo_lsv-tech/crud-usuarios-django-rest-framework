## Users CRUD REST Framework

### Installation

- python3 -m venv env_name
- source env_name/bin/activate
- sudo apt install git (if you don't have git installed)
- git clone git@gitlab.com:Ricardo_lsv-tech/crud-usuarios-django-rest-framework.git (by ssh)
- git clone https://gitlab.com/Ricardo_lsv-tech/crud-usuarios-django-rest-framework.git (by http)
- cd crud-usuarios-django-rest-framework
- pip install -r requirements.txt
- python manage.py makemigrations
- python manage.py migrate
- python manage.py createsuperuser
- python manage.py runserver

### MEMBERS
1. **ANDRES FELIPE MOLINARES BOLAÑOS**
2. **RICARDO ENRIQUE JARAMILLO ACEVEDO**